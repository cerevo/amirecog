from logging import getLogger
logger = getLogger(__name__)

import wiringpi as w
import time
import queue
import threading

LED_R = 26
LED_G = 13
LED_B = 19

def get_pin_from_color(color):
    if color == 'r' or color == 'red':
        return LED_R
    elif color == 'g' or color == 'green':
        return LED_G
    elif color == 'b' or color == 'blue':
        return LED_B


w.wiringPiSetup()
io = w.GPIO(w.GPIO.WPI_MODE_GPIO_SYS)
io.pinMode(26, io.OUTPUT)
io.pinMode(19, io.OUTPUT)
io.pinMode(13, io.OUTPUT)


class Led(object):
    def on(self, color='r'):
        pin = get_pin_from_color(color)
        io.digitalWrite(pin, 1)

    def off(self, color='r'):
        pin = get_pin_from_color(color)
        io.digitalWrite(pin, 0)

    def blink(self, color='r', interval=0.3):
        b = LedBlink(color, interval)
        b.start()
        return b

class LedBlink(threading.Thread):
    def __init__(self, color, interval):
        self.color = color
        self.interval = interval
        self.q = queue.Queue()
        super().__init__()

    def run(self):
        pin = get_pin_from_color(self.color)
        while 1:
            try:
                cmd = self.q.get_nowait()
            except queue.Empty as e:
                pass
            else:
                if cmd == 'stop':
                    io.digitalWrite(pin, 0)
                    break

            io.digitalWrite(pin, 1)
            time.sleep(self.interval)
            io.digitalWrite(pin, 0)
            time.sleep(self.interval)

    def stop(self):
        self.q.put('stop')
        time.sleep(self.interval * 2)


if __name__ == '__main__':
    led = Led()

    led.on('r')
    time.sleep(3)
    led.off('r')

    blink = led.blink('b', interval=0.5)

    time.sleep(5)
    blink.stop()

