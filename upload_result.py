# encoding: utf-8

from logging import getLogger, StreamHandler, Formatter, DEBUG
from logging import StreamHandler, FileHandler
logger = getLogger(__name__)
#handler = StreamHandler()
handler = FileHandler(filename="amirecog.log", encoding='utf-8')
handler.setLevel(DEBUG)
handler.setFormatter(Formatter("%(asctime)s %(levelname)8s %(message)s"))
logger.setLevel(DEBUG)
logger.addHandler(handler)
logger.propagate = False

import base64
import time
import os
import json
import fileinput
from urllib import parse
import requests
import subprocess
import tempfile
from multiprocessing import Process

import led

import wave
from io import BytesIO

conf = json.loads(open("conf.json", "r").read())

def error_led(interval=0.3):
    l = led.Led()
    eblink = l.blink('r', interval=interval)
    time.sleep(2)
    eblink.stop()

def create_wav(audio_file_path):
    frames = open(audio_file_path, 'rb').read()

    wav_data = BytesIO()
    wav_file = wave.open(wav_data, 'wb')
    wav_file.setnchannels(1)
    wav_file.setframerate(16000)
    wav_file.setsampwidth(2)
    wav_file.writeframes(frames)

    return wav_data

def get_new_id():
    bridgeid = None
    blink = l.blink('g')
    try:
        new_id_url = parse.urljoin(server_base_url, "/api/get_new_id")
        id_json = json.loads(requests.post(new_id_url, data=json.dumps({"access_key": conf["accessKey"]})).text)
        bridgeid = id_json['id']
        logger.debug("got_new_id: {}".format(bridgeid))
    except Exception as e:
        logger.exception(e)
        blink.stop()
        error_led()
        bridgeid = None
    else:
        blink.stop()
    return bridgeid

def upload_vr_result(bridgeid, vrresult):
    blink = l.blink('g')
    wave_content = None
    try:
        logger.debug("uploading vr result: {}".format(vrresult))
        new_vrresult_url = parse.urljoin(server_base_url, "/api/upload_vr_result?id={}".format(str(bridgeid)))
        vrresult["access_key"] = conf["accessKey"]
        response = requests.post(new_vrresult_url, data=json.dumps(vrresult))
        if response.status_code != 200:
            logger.debug("response code error: {}".format(response.status_code))
            raise Exception("status code error")
        wave_content = response.content
    except Exception as e:
        logger.exception(e)
        blink.stop()
        error_led()
        wave_content = None
    else:
        blink.stop()
    return wave_content

def wave_save_and_play(wave_content):
    with tempfile.NamedTemporaryFile() as fp:
        fp.write(wave_content)
        fp.flush()
        aplay_cmd = "aplay -q -D plughw:0,0 {}".format(fp.name)
        logger.debug("aplay_cmd: {}".format(aplay_cmd))
        subprocess.call([aplay_cmd,], shell=True)

        # audio.rawを削除
        try:
            os.unlink(audio_file_path)
        except Exception as e:
            logger.exception(e)

        logger.debug("save,play,remove wave done.")

def upload_wave(bridgeid, audio_file_path):
    blink = l.blink('g')
    try:
        logger.debug("uploading audio wav: {}".format(audio_file_path))
        audio_wav = create_wav(audio_file_path)
        upload_resultwav_url = parse.urljoin(server_base_url, "/api/upload_wave?id={}".format(str(bridgeid)))
        post_values = {"access_key": conf["accessKey"], "req_voice": base64.urlsafe_b64encode(audio_wav.getvalue()).decode("utf-8")}
        response = requests.post(upload_resultwav_url, data=json.dumps(post_values))
        if response.status_code != 200:
            logger.debug("response code error: {}".format(response.status_code))
            raise Exception("status code error")
    except Exception as e:
        logger.exception(e)
        blink.stop()
        error_led()
    else:
        blink.stop()


if __name__ == "__main__":
    conf = json.loads(open('conf.json').read())
    server_base_url = conf['serverBaseUrl']

    l = led.Led()
    l.off('r')
    l.off('g')
    l.off('b')

    l.on('b')
    first = 0
    stdinput = fileinput.input()
    while 1:
        l.on('b')
        line = stdinput.readline()

        # 1行目はAmiVoice SDKのライブラリからの出力なので無視
        if first == 0:
            first = 1
            continue

        logger.debug("received line: {}".format(line))
        l.off('b')

        blink = l.blink('b', interval=0.1)
        try:
            vrresult = json.loads(line)
        except Exception as e:
            logger.exception(e)
            blink.stop()
            error_led(interval=0.1)
            continue

        if int(vrresult["statuscode"]) != 0:
            logger.info("error msg: {}".format(vrresult["message"]))
            blink.stop()
            error_led(interval=0.1)
            continue

        audio_file_path = vrresult['audio_file']
        del vrresult['audio_file']
        blink.stop()

        blink = l.blink('b')
        bridgeid = get_new_id()
        if bridgeid is None:
            continue
        wave_content = upload_vr_result(bridgeid, vrresult)
        if wave_content is None:
            continue
        p = Process(target=wave_save_and_play, args=(wave_content,))
        p.start()
        upload_wave(bridgeid, audio_file_path)
        p.join()
        blink.stop()

