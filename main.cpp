#include <stdio.h>
#include <time.h>
#include <string.h>
#include <locale.h>
#include <unistd.h>

#include "IHandsfreeControl.h"

#include <iostream>
#include <fstream>
#include <map>
#include <cassert>
#include "picojson.h"

#include <sys/socket.h> //socket(), bind(), accept(), listen()
#include <arpa/inet.h> // struct sockaddr_in, struct sockaddr, inet_ntoa()
#include <stdlib.h> //atoi(), exit(), EXIT_FAILURE, EXIT_SUCCESS

using namespace std;

#define QUEUELIMIT 5

static IVoiceRecognizerOutputStream* _sVoiceRecognizer;
static IHandsfreeControl* _sHandsfreeControl;
void recognize() ;

char fname[64];
picojson::value conf;

static void VOICE_RECOGNIZER_API_CALL complete(const void* sender, const char* values) {
	if (values == NULL) return;
	string json = values;

	picojson::object& oconf = conf.get<picojson::object>();

	picojson::value v;
	std::string err = picojson::parse(v, json);
	if (err.empty()) {
		picojson::object& o = v.get<picojson::object>();
		if (((int)o["statuscode"].get<double>()) == 0) {
			// 認識結果の送信者がIHandsfreeControlの場合
			if (sender == _sHandsfreeControl) {
				// トリガーワードを認識した際に
				std::string audio_file_path = oconf["audioFileDir"].get<std::string>() + std::string("audio_%y%m%d%H%I%S.raw");
				time_t t = time(NULL);
				strftime(fname, sizeof(fname), audio_file_path.c_str(), localtime(&t));

				int buffersize = ((IHandsfreeControl*)sender)->getUtteranceLength(false);
				char* buffer = new char[buffersize];
				((IHandsfreeControl*)sender)->getUtteranceAudioData(buffer);
				std::ofstream audioFile;
				audioFile.open(fname, std::ios::out|ios::binary);
				audioFile.write(buffer, buffersize);
				audioFile.close();
				delete buffer;

				// トリガーワード以降の発話時間がある程度ある場合に、サーバー認識を行います（この例では600m秒）
				// トリガーワード以降発話時間があまりに少ない場合に認識を行うと、
				// 誤認識による誤動作が増える可能性があります
				int utteranceLength = ((IHandsfreeControl*)sender)->getUtteranceLength(true);
				if (utteranceLength >= 200) {
					_sVoiceRecognizer = createVoiceRecognizerOutputStream();
					_sVoiceRecognizer->complete = complete;

					TCHAR *idbuf = new TCHAR[1024];
					TCHAR *passbuf = new TCHAR[1024];
					memset(idbuf, 0, sizeof(idbuf));
					std::sprintf(idbuf, "%s", oconf["serviceId"].get<std::string>().c_str());
					_sVoiceRecognizer->serviceId = _T(idbuf);

					memset(passbuf, 0, sizeof(passbuf));
					std::sprintf(passbuf, "%s", oconf["servicePassword"].get<std::string>().c_str());
					_sVoiceRecognizer->servicePassword = _T(passbuf);
					((IHandsfreeControl*)sender)->recognizeRestUtterance(_sVoiceRecognizer);
				}
			}
			else { // _sVoiceRecognizerでの認識
				picojson::object& o = v.get<picojson::object>();
				o.insert(std::map<string, string>::value_type("audio_file", fname));
				std::cout << v << std::endl;
				std::cout << std::flush;
				delete _sVoiceRecognizer;
				_sVoiceRecognizer = 0;
			}
		} else {
			std::cout << v << std::endl;
			std::cout << std::flush;
		}
	} else {
	 std::cerr << err << std::endl;
    }
}

static void VOICE_RECOGNIZER_API_CALL utterance(const void* sender, bool started) {
	//std::cerr << "Utterance Event" << started << std::endl;
}

vector<string> loadTriggerWords(const char* filename) {
	vector<string> triggerWords;
	ifstream ifs(filename, ios::in);
	string buf;
	while(ifs && getline(ifs, buf)) {
		triggerWords.push_back(buf);
	}
	return triggerWords;
}

int main() {
	// ロケールの設定
	setlocale(LC_ALL, "ja_JP.UTF-8");

	std::ifstream fs;
	fs.open("conf.json", std::ios::binary);
	assert(fs);
	fs >> conf;
	fs.close();

	vector<string> triggerWords = loadTriggerWords("./triggerwords.txt");
	// vectorの内容をcharの２次元配列に変換
	const char** tw = new const char*[(triggerWords.size() + 1) * sizeof(char*)];
	for(size_t i = 0, size = triggerWords.size(); i < size; ++i) {
		tw[i] = triggerWords[i].c_str();
	}
	// tw[triggerWords.size()]にはデリミッタとしてNULLを入れて下さい。
	tw[triggerWords.size()] = NULL;

	_sHandsfreeControl = createHandsfreeControl();
	_sHandsfreeControl->complete = complete;
	//_sHandsfreeControl->updated = complete;
	_sHandsfreeControl->utterance = utterance;

	_sHandsfreeControl->triggerWords = tw;
	picojson::object& oconf = conf.get<picojson::object>();

	TCHAR *idbuf = new TCHAR[1024];
	TCHAR *passbuf = new TCHAR[1024];
	TCHAR *segbuf = new TCHAR[1024];
	TCHAR *engbuf = new TCHAR[1024];
	memset(idbuf, 0, sizeof(idbuf));
	std::sprintf(idbuf, "%s", oconf["serviceId"].get<std::string>().c_str());
	_sHandsfreeControl->serviceId = _T(idbuf);

	memset(passbuf, 0, sizeof(passbuf));
	std::sprintf(passbuf, "%s", oconf["servicePassword"].get<std::string>().c_str());
	_sHandsfreeControl->servicePassword = _T(passbuf);

	memset(segbuf, 0, sizeof(segbuf));
	std::sprintf(segbuf, "%s", oconf["segmenterProperties"].get<std::string>().c_str());
	_sHandsfreeControl->segmenterProperties = _T(segbuf);

	memset(engbuf, 0, sizeof(engbuf));
	std::sprintf(engbuf, "%s", oconf["enginemodePath"].get<std::string>().c_str());
	_sHandsfreeControl->enginemodePath = _T(engbuf);

	// デバッグ用ログ出力をONにする
	// 標準出力にデバッグ用ログが出力されますので、通常はOFFでご使用ください
	//_sHandsfreeControl->outputDebugLog = true;

        int servsock;
        int clientsock;
        struct sockaddr_in servsockaddr;
        struct sockaddr_in clientsockaddr;
        unsigned short servport = 5920;
        unsigned int clientlen;

        if ((servsock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ){
		perror("socket() failed.");
		exit(EXIT_FAILURE);
        }

        memset(&servsockaddr, 0, sizeof(servsockaddr));
        servsockaddr.sin_family = AF_INET;
        servsockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servsockaddr.sin_port = htons(servport);

        if (bind(servsock, (struct sockaddr *) &servsockaddr, sizeof(servsockaddr) ) < 0 ) {
		perror("bind() failed.");
		exit(EXIT_FAILURE);
        }

        if (listen(servsock, QUEUELIMIT) < 0) {
		perror("listen() failed.");
		exit(EXIT_FAILURE);
        }

	if (_sHandsfreeControl->setup() == true) {
		std::cout << std::flush;
	}

	_sHandsfreeControl->start();
        while(1) {
		clientlen = sizeof(clientsockaddr);
		if ((clientsock = accept(servsock, (struct sockaddr *) &clientsockaddr, &clientlen)) < 0) {
			perror("accept() failed.");
			exit(EXIT_FAILURE);
		}

		printf("connected from %s.\n", inet_ntoa(clientsockaddr.sin_addr));
		close(clientsock);
		break;
	}
	_sHandsfreeControl->cancel();

	delete _sHandsfreeControl;
	_sHandsfreeControl = 0;
	delete tw;

	return 0;
}

