# encoding: utf-8

import base64
import os
from sanic import Sanic
from sanic.response import json,raw

AUDIO_DIR = './audiofiles'

from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase

db = SqliteExtDatabase('vr_result.db')

import uuid
class BaseModel(Model):
    class Meta:
        database = db

class VrResult(BaseModel):
    bridgeid = UUIDField(unique=True)
    result = TextField(null=True)
    wavfilepath = TextField(null=True)

db.connect()
try:
    db.create_tables([VrResult,])
except:
    import traceback
    traceback.print_exc()

app = Sanic()

@app.route("/")
async def test(request):
    return json({"hello": "world"})

@app.route("/api/get_new_id", methods=("POST",))
async def new_id(request):
    print("access_token: {}".format(request.json["access_key"]))

    uid = uuid.uuid1()
    r = VrResult.create(bridgeid=uid, result=None, wavfilepath=None)
    return json({"id": str(r.bridgeid)})

@app.route("/api/upload_vr_result", methods=("POST",))
async def new_vrresult(request):
    uid = request.args["id"][0]
    print("bridge id: {}".format(uid))

    vrr = VrResult.select().where(VrResult.bridgeid == uuid.UUID(uid)).get()
    vrr.result = str(request.body)
    vrr.save()

    return raw(open("dummy.wav","rb").read())

@app.route("/api/upload_wave", methods=("POST",))
async def upload_resultwav(request):
    uid = request.args["id"][0]
    print("bridge id: {}".format(uid))

    vrr = VrResult.select().where(VrResult.bridgeid == uuid.UUID(uid)).get()
    wav = base64.urlsafe_b64decode(request.json["req_voice"])
    open(os.path.join(AUDIO_DIR, str(vrr.bridgeid) + '.wav'), 'wb').write(wav)

    return json({})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
