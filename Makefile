TARGET1=rtw

PLATFORM=raspbian

buildtype := release

INCLUDEPATH=-I./ -I../../include

SRCS=\
	./main.cpp

LIBS =-lAmiVoiceHandsfreeControl -lAmiVoiceRecognizer -lxcwrapper

CXX = g++
CXXFLAGS1=-DLINUX -Wno-write-strings -Wno-psabi
LDFLAGS = -L../../lib/$(PLATFORM)


ifeq ($(buildtype),release)
  CXXFLAGS += -O3
else ifeq ($(buildtype),debug)
  CXXFLAGS += -O0 -g
else
  $(error buildtype must be release, debug, profile or coverage)
endif


OUTDIR := Build/$(buildtype)

PROG1 := $(OUTDIR)/$(TARGET1)

OBJS1 = $(SRCS:%.cpp=$(OUTDIR)/record/%.o)

DEPS1 = $(SRCS:%.cpp=$(OUTDIR)/record/%.d)

.PHONY: install clean distclean

all: $(PROG1)

-include $(DEPS1)

$(PROG1): $(OBJS1)
	$(CXX) $(LDFLAGS)  -o $@ $^ $(LIBS)
	@echo done.

$(OUTDIR)/record/%.o:%.cpp
	@if [ ! -e `dirname $@` ]; then mkdir -p `dirname $@`; fi
	$(CXX) $(CXXFLAGS1) $(INCLUDEPATH) $(DEFS) -o $@ -c -MMD -MP -MF $(@:%.o=%.d) $<

clean:
	rm -rf $(OUTDIR)

distclean:
	rm -rf Build
